package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/kylehqcom/slate"
)

var (
	pl = fmt.Println
	sf = fmt.Sprintf
)

func main() {
	pl("Thanks for using 'slate' =] \n")
	pl("Watch the console as scheduled entries are invoked and stats are displayed\n")

	s := slate.NewSchedule(slate.NewScheduleConfig(slate.WithFlushEvery(time.Second * 7)))
	e := slate.ScheduleEntry{
		Fn:      func() { pl(sf("Schedule Entry Fn invoked: %s\n", time.Now().Format(time.RFC1123Z))) },
		Begins:  time.Now().Add(time.Second * 4),
		Every:   time.Second * 2,
		Expires: time.Now().Add(time.Second * 10),
	}
	ID, err := s.Submit(e)
	if err != nil {
		log.Fatal(err)
	}

	pl(sf("Submitted a new schedule entry with ID: %s \n", ID))

	ticker := time.NewTicker(time.Second * 3)
	stop := time.Now().Add(time.Second * 15)
	for t := range ticker.C {
		pl(sf("Schedule Stats: %+v", s.Stats()))
		pl(sf("Entry Stats: %+v \n", s.EntryStats(ID)))
		if t.After(stop) {
			break
		}
	}

	pl("An entry has fired and expired and the scheduled has been flushed of expired/completed entries.\n")
	pl("Example complete.")
}
