package slate

import "sync"

type (
	// Manager is a convenience struct to manage many Schedules
	Manager struct {
		Schedules Schedules
		sync.Mutex
	}

	// Schedules is a map of Schedule instances on a Manager
	Schedules map[string]*Schedule
)

// NewManager will create a new schedule manager. Managers are simply a wrapper for multiple
// schedule to help callers organise their workflow. If you only require a single schedule, then
// call NewSchedule() and work with directly.
func NewManager() *Manager {
	return &Manager{
		Schedules: map[string]*Schedule{},
	}
}

// Add will add a schedule to this manager.
func (m *Manager) Add(name string, s *Schedule) {
	m.Lock()
	m.Schedules[name] = s
	m.Unlock()
}

// All will return all Schedules on this manager.
func (m *Manager) All() Schedules {
	return m.Schedules
}

// Drop will stop and remove all related jobs on the schedule
// before removing the schedule from this manager.
func (m *Manager) Drop(name string) {
	s, _ := m.Get(name)
	if s != nil {
		s.Reset()
		// Also stop any flushers to release resources
		if s.flusher.ticker != nil {
			s.flusher.ticker.Stop()
			close(s.flusher.done)
		}
	}

	delete(m.Schedules, name)
}

// Get will return by name a schedule instance from this manager.
func (m *Manager) Get(name string) (*Schedule, error) {
	m.Lock()
	defer m.Unlock()
	s, ok := m.Schedules[name]
	if ok {
		return s, nil
	}

	return nil, ErrScheduleNotFound
}

// Reset will stop and remove all Schedules along with their associated entries.
func (m *Manager) Reset() {
	for k, s := range m.All() {
		s.Reset()
		delete(m.Schedules, k)
	}
}
