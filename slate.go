// Package slate is an in memory non persistent scheduler that simplifies
// handling repeat method calls. slate allows you to
//
//  - Run a job n number of times
//  - At a predefined start time
//  - At every n unit of time (nano, micro, milli seconds, minutes...)
//  - With automatic expirations
//
// slate also keeps track of these jobs running asynchronously in go routines
// so you can forcefully stop them at anytime.
package slate
