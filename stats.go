package slate

import (
	"time"
)

type (
	// ScheduleStats return stats about a Schedule instance
	ScheduleStats struct {
		Config struct {
			AllowDuplicateEntryID bool
			EntryRetention        time.Duration
			FlushEvery            time.Duration
		}
		EntryCount       int
		CapBreachedCount int
		ErrorCount       int
		ExpiredCount     int
		PendingCount     int
		RunningCount     int
		StoppedCount     int
	}

	// ScheduleEntryStats return stats about a Schedule Entry instance
	ScheduleEntryStats struct {
		Completed    time.Time
		Err          error
		RunningCount uint64
		Started      time.Time
		State        string
		Stopped      time.Time
	}
)

// generateScheduleStats to return internal schedule info. Note that
// creating stats locks the schedule fo counts so be mindful of over calling.
func generateScheduleStats(s *Schedule) ScheduleStats {
	stats := ScheduleStats{}
	stats.Config.AllowDuplicateEntryID = !s.Config.DisallowDuplicateEntryID
	stats.Config.EntryRetention = s.Config.EntryRetention
	stats.Config.FlushEvery = s.Config.FlushEvery

	s.Lock()
	defer s.Unlock()

	stats.EntryCount = len(s.runners)
	for _, r := range s.runners {
		r.Lock()
		defer r.Unlock()
		switch r.state {
		case stateCapBreached:
			stats.CapBreachedCount++
		case stateError:
			stats.ErrorCount++
		case stateExpired:
			stats.ExpiredCount++
		case statePending:
			stats.PendingCount++
		case stateRunning:
			stats.RunningCount++
		case stateStopped:
			stats.StoppedCount++
		}
	}

	return stats
}

// generateScheduleEntryStats to return internal entry running info. Note that
// creating stats locks the schedule fo counts so be mindful of over calling.
func generateScheduleEntryStats(r *runner) ScheduleEntryStats {
	stats := ScheduleEntryStats{}
	if r == nil {
		stats.Err = ErrScheduleEntryNotFound
		stats.State = string(stateError)
		return stats
	}

	r.Lock()
	defer r.Unlock()
	stats.Completed = r.completed
	stats.Err = r.err
	stats.RunningCount = r.runningCount
	stats.Started = r.started
	stats.State = string(r.state)
	stats.Stopped = r.stopped
	return stats
}
