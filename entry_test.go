package slate_test

import (
	"testing"
	"time"

	"github.com/matryer/is"
	. "gitlab.com/kylehqcom/slate"
)

func TestGenerateEntryID(t *testing.T) {
	is := is.New(t)
	ID := GenerateEntryID()
	is.Equal(32, len(ID))

	custom := "foobar"
	ID = GenerateEntryID(custom)
	is.Equal(len(custom), len(ID))
	is.Equal(custom, string(ID))
}

func TestStats(t *testing.T) {
	is := is.New(t)
	e := ScheduleEntry{
		Cap:   10,
		Fn:    func() {},
		ID:    GenerateEntryID("simple"),
		Every: time.Duration(time.Millisecond * 200),
	}

	s := NewSchedule()
	_, err := s.Submit(e)
	if err != nil {
		t.Error("Unable to submit simple test stat entry", err)
	}
	stats := s.EntryStats(e.ID)
	is.True(stats.Completed.IsZero())
	is.True(stats.Started.IsZero())
	is.True(stats.Stopped.IsZero())
	is.Equal(nil, stats.Err)
	is.Equal(uint64(0), stats.RunningCount)
	is.Equal("pending", string(stats.State))

	// Also check that we have an error on no entry found
	stats = s.EntryStats(GenerateEntryID("anything"))
	is.Equal(ErrScheduleEntryNotFound, stats.Err)
	is.Equal("error", string(stats.State))
}
