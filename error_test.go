package slate

import (
	"errors"
	"testing"
)

func TestIsSlateError(t *testing.T) {
	if !IsSlateError(ErrScheduleNotFound) {
		t.Error("Expected a slate error.")
	}

	e := new(error)
	if IsSlateError(*e) {
		t.Error("Any other error is not a slate error.")
	}

	if IsSlateError(errors.New("http://www.kylehq.com")) {
		t.Error("New error is not a slate error.")
	}

	if ErrScheduleNotFound.Error() == "" {
		t.Error("Errors should return a valid error message.")
	}
}
