package slate_test

import (
	"testing"
	"time"

	"github.com/matryer/is"
	. "gitlab.com/kylehqcom/slate"
)

func simple() {}
func newSimple() ScheduleEntry {
	return ScheduleEntry{
		Cap:   10,
		Fn:    simple,
		ID:    GenerateEntryID("simple"),
		Every: time.Duration(time.Millisecond * 200),
	}
}

func withParam(param string) {}
func newWithParam() ScheduleEntry {
	return ScheduleEntry{
		Cap:   10,
		Fn:    func() { withParam("www.kylehq.com") },
		ID:    GenerateEntryID("param"),
		Every: time.Duration(time.Second),
	}
}

func TestScheduleSubmit(t *testing.T) {
	is := is.New(t)

	// Assert dupe id error
	c := NewScheduleConfig(WithDisallowDuplicateEntryID(true))
	s := NewSchedule(c)
	_, err := s.Submit(newSimple())
	is.NoErr(err)

	_, err = s.Submit(newSimple())
	is.True(err != nil)
	is.Equal(ErrScheduleEntryDuplicateID, err)

	// Assert zero every error
	s = NewSchedule()
	emptyEntry := ScheduleEntry{}
	_, err = s.Submit(emptyEntry)
	is.True(err != nil)
	is.Equal(ErrDurationZero, err)

	// Assert empty fn
	emptyEntry.Every = time.Minute
	_, err = s.Submit(emptyEntry)
	is.True(err != nil)
	is.Equal(ErrInvalidFunction, err)

	// Assert bad fn
	emptyEntry.Fn = nil
	_, err = s.Submit(emptyEntry)
	is.True(err != nil)
	is.Equal(ErrInvalidFunction, err)

	// Assert expiration err in past
	e := newSimple()
	e.Expires = time.Now().Add(-1 * time.Minute)
	_, err = s.Submit(e)
	is.True(err != nil)
	is.Equal(ErrExpirationInPast, err)

	// Assert Begins/Now + Every is less than expires
	e = newSimple()
	e.Expires = time.Now().Add(time.Minute)
	e.Every = 2 * time.Minute
	_, err = s.Submit(e)
	is.True(err != nil)
	is.Equal(ErrInvalidTiming, err)

	// Assert wrapped function
	e = newWithParam()
	id, err := s.Submit(e)
	is.NoErr(err)
	is.Equal(id.String(), "param")
}

func TestScheduleRemove(t *testing.T) {
	is := is.New(t)
	s := NewSchedule()
	ID, err := s.Submit(newSimple())
	is.NoErr(err)
	stats := s.Stats()
	is.Equal(1, stats.EntryCount)
	is.Equal(1, stats.PendingCount)

	s.Drop(ID)

	stats = s.Stats()
	is.Equal(0, stats.EntryCount)
	is.Equal(0, stats.PendingCount)
}

func TestScheduleReset(t *testing.T) {
	is := is.New(t)
	s := NewSchedule()
	_, err := s.Submit(newSimple())
	is.NoErr(err)

	stats := s.Stats()
	is.Equal(1, stats.EntryCount)
	is.Equal(1, stats.PendingCount)

	s.Reset()

	stats = s.Stats()
	is.Equal(0, stats.EntryCount)   // Entry count should be 0
	is.Equal(0, stats.PendingCount) // Pending count should be 0
}

func TestScheduleFlush(t *testing.T) {
	is := is.New(t)
	sc := NewScheduleConfig(WithFlushEvery(time.Millisecond * 3))
	s := NewSchedule(sc)
	ID, err := s.Submit(newSimple())
	is.NoErr(err)

	stats := s.Stats()
	is.Equal(1, stats.EntryCount)
	is.Equal(1, stats.PendingCount)

	// Call stop so that we can confirm flush cleanup
	s.Drop(ID)
	is.NoErr(err)

	time.Sleep(time.Millisecond * 10)

	stats = s.Stats()
	is.Equal(0, stats.EntryCount)
}

func TestScheduleRun(t *testing.T) {
	is := is.New(t)
	s := NewSchedule()
	_, err := s.Submit(newSimple())
	is.NoErr(err)

	stats := s.Stats()
	is.Equal(1, stats.EntryCount)
	is.Equal(1, stats.PendingCount)

	time.Sleep(time.Millisecond * 250)

	stats = s.Stats()
	is.Equal(1, stats.EntryCount)
	is.Equal(0, stats.PendingCount)
	is.Equal(1, stats.RunningCount)
}
