package slate_test

import (
	"testing"

	"github.com/matryer/is"

	. "gitlab.com/kylehqcom/slate"
)

func TestManagerSchedules(t *testing.T) {
	is := is.New(t)
	m := NewManager()
	is.Equal(0, len(m.All())) // a new manager should have no schedules

	m.Add("foo", NewSchedule())
	is.Equal(1, len(m.All())) // add schedule should set len 1

	_, err := m.Get("foo")
	is.NoErr(err) // get foo schedule should no return an error

	_, err = m.Get("bar")
	is.Equal(err, ErrScheduleNotFound)

	m.Reset()
	is.Equal(0, len(m.All()))

	m.Add("foo", NewSchedule())
	is.Equal(1, len(m.All())) // add schedule should set len 1
	m.Drop("foo")
	is.Equal(0, len(m.All())) // drop schedule should leave len 0
}
