package slate

import (
	"sync"
	"time"
)

type (
	// Schedule is the default struct that houses job runners.
	Schedule struct {
		Config  ScheduleConfig
		flusher struct {
			done   chan struct{}
			ticker *time.Ticker
		}
		runners runners
		sync.Mutex
	}

	// ScheduleConfigOption is a func used to bind schedule config options
	ScheduleConfigOption func(*ScheduleConfig)

	// ScheduleConfig contains all available config options for a schedule
	ScheduleConfig struct {
		DisallowDuplicateEntryID bool
		FlushEvery               time.Duration
		EntryRetention           time.Duration
	}
)

// NewSchedule will return a new Scheduler instance. If nil config is passed, the defaults are used.
func NewSchedule(cfg ...ScheduleConfig) *Schedule {
	c := NewScheduleConfig()
	if len(cfg) > 0 {
		c = cfg[0]
	}

	// Create a new schedule, add to the schedulePool.
	s := &Schedule{
		Config:  c,
		runners: make(runners),
	}

	// Setup flushing for this schedule if flush duration non zero
	if c.FlushEvery.Nanoseconds() != 0 {
		s.flusher = struct {
			done   chan struct{}
			ticker *time.Ticker
		}{
			done:   make(chan struct{}),
			ticker: time.NewTicker(c.FlushEvery),
		}
		go func() {
			for {
				select {
				case <-s.flusher.done:
					return
				case <-s.flusher.ticker.C:
					s.flush()
				}
			}
		}()
	}

	return s
}

// NewScheduleConfig will return a Config for a schedule. Takes variadic ScheduleConfigOption
func NewScheduleConfig(opts ...ScheduleConfigOption) ScheduleConfig {
	sc := &ScheduleConfig{}
	for _, opt := range opts {
		opt(sc)
	}
	return *sc
}

// WithDisallowDuplicateEntryID will bind the config option to (dis)allow duplicate schedule entry IDs
func WithDisallowDuplicateEntryID(disallow bool) ScheduleConfigOption {
	return func(c *ScheduleConfig) {
		c.DisallowDuplicateEntryID = disallow
	}
}

// WithEntryRetention will bind the config option to retain entries on a schedule.
func WithEntryRetention(retention time.Duration) ScheduleConfigOption {
	return func(c *ScheduleConfig) {
		c.EntryRetention = retention
	}
}

// WithFlushEvery will bind the config option to flush completed/stopped schedule entries.
func WithFlushEvery(every time.Duration) ScheduleConfigOption {
	return func(c *ScheduleConfig) {
		c.FlushEvery = every
	}
}

// addRunner to this schedule instance. Method ensures locks.
func (s *Schedule) addRunner(ID EntryID, r *runner) {
	s.Lock()
	defer s.Unlock()
	s.runners[ID] = r
}

// findRunner will attempt to find and return a Runner instance on this schedule by EntryID.
func (s *Schedule) findRunner(ID EntryID) (*runner, error) {
	s.Lock()
	defer s.Unlock()
	r, ok := s.runners[ID]
	if ok {
		return r, nil
	}

	return nil, ErrScheduleEntryNotFound
}

// flush is responsible for the removal of completed runners from a schedule.
func (s *Schedule) flush() {
	d := time.Now().UTC().Add(-1 * s.Config.EntryRetention)
	s.Lock()
	defer s.Unlock()
	for _, r := range s.runners {
		if (r.isCompleted() && r.completed.Before(d)) || r.isStopped() {
			delete(s.runners, r.entry.ID)
		}
	}
}

// Drop will first stop any job if applicable and then remove the entry from this schedule.
func (s *Schedule) Drop(ID EntryID) {
	r, _ := s.findRunner(ID)
	if r != nil {
		r.stop()
	}
	s.Lock()
	delete(s.runners, ID)
	s.Unlock()
}

// EntryStats will return a snapshot of stats for a scheduleEntry on this schedule instance.
// Note that this will lock the internal runner so be mindful of multiple calls
func (s *Schedule) EntryStats(ID EntryID) ScheduleEntryStats {
	r, _ := s.findRunner(ID)
	return generateScheduleEntryStats(r)
}

// Reset will stop and remove ALL jobs from this schedule.
func (s *Schedule) Reset() {
	for _, r := range s.runners {
		s.Drop(r.entry.ID)
	}
}

// Stats will return a snapshot of this schedules running events.
// Note that this will lock the schedule so be mindful of multiple calls
func (s *Schedule) Stats() ScheduleStats {
	return generateScheduleStats(s)
}

// Submit will add & run a new entry to the current schedule.
func (s *Schedule) Submit(e ScheduleEntry) (EntryID, error) {
	if e.ID == "" {
		e.ID = GenerateEntryID()
	}

	// Check for an existing runner with the same ID
	s.Lock()
	existing, ok := s.runners[e.ID]
	s.Unlock()
	if ok {
		// Are duplicate ids allowed, eg overrides
		if s.Config.DisallowDuplicateEntryID {
			return e.ID, ErrScheduleEntryDuplicateID
		}

		// We have an existing entry to override so stop this existing runner
		existing.stop()
	}

	// Check we have a valid Every duration.
	if e.Every.Nanoseconds() == 0 {
		return e.ID, ErrDurationZero
	}

	if !e.Expires.IsZero() {
		// Check Expires is not in the past
		if e.Expires.Before(time.Now()) {
			return e.ID, ErrExpirationInPast
		}

		// Check if the entry will be Expired on first iteration
		from := e.Begins
		if e.Begins.IsZero() || time.Now().After(e.Begins) {
			from = time.Now()
		}

		if from.Add(e.Every).After(e.Expires) {
			return e.ID, ErrInvalidTiming
		}
	}

	// Create the runner which handles the method call. Note that this function receives a
	// ScheduleEntry value, but from here on, we use a pointer for internal processing.
	validatedEntry := &e
	r, err := createRunner(validatedEntry)
	if err != nil {
		return validatedEntry.ID, err
	}

	// With a valid runner we can bind this schedule and submit for running
	validatedEntry.schedule = s
	s.addRunner(validatedEntry.ID, r)
	r.run()
	return validatedEntry.ID, nil
}
