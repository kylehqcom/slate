package slate

//
// Note that a runner and its fields are all private in the
// slate package. So this test file namespace is slate, not slate_test.
//

import (
	"testing"
	"time"

	"github.com/matryer/is"
)

func TestCanBegin(t *testing.T) {
	is := is.New(t)
	e := &ScheduleEntry{
		Begins: time.Now().Add(time.Minute),
	}
	r := runner{entry: e}
	is.True(!r.canBegin()) // Can begin should be false with date 1 mins in future.

	e.Begins = time.Now().Add(-100 * time.Millisecond)
	is.True(r.canBegin()) // Can begin should be true with date in past.
}

func TestCapBreached(t *testing.T) {
	is := is.New(t)
	e := &ScheduleEntry{
		Cap: 0,
	}
	r := runner{entry: e}
	is.True(!r.capBreached()) // Cap should not be breached with Cap of zero.

	e.Cap = 1
	r.runningCount = 1
	is.True(r.capBreached()) // Cap should be breached with running count 1.

	r.runningCount = 2
	is.True(r.capBreached()) // Cap should be breached with running count 2.
}

func TestIsCompleted(t *testing.T) {
	is := is.New(t)
	r := runner{}
	is.True(!r.isCompleted()) // Is completed should be false with zero time.

	r.completed = time.Now()
	is.True(r.isCompleted()) // Is completed should be true with a time set.
}

func TestIsExpired(t *testing.T) {
	is := is.New(t)
	e := &ScheduleEntry{
		Expires: time.Time{},
	}
	r := runner{entry: e}
	is.True(!r.isExpired()) // Expired should be false as zero expire time assigned to entry.

	e.Expires = time.Now().Add(time.Minute)
	is.True(!r.isExpired()) // Expired should be false as expiration one minute from now.

	e.Expires = time.Now().Add(-2 * time.Minute)
	is.True(r.isExpired()) // Expired should be true as expiration in past.
}

func TestIsStopped(t *testing.T) {
	is := is.New(t)
	r := runner{}
	is.True(!r.isStopped()) // A new runner should be in a stopped status.

	r.stop()
	is.True(r.isStopped())          // Runner should be in a stopped state after stop called.
	is.Equal(r.state, stateStopped) // Runner should have stopped state assigned.
}
