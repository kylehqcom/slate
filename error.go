package slate

// slateError is the package level error struct.
type slateError struct {
	s string
}

var (
	// ErrDurationZero occurs when adding a new job, but the every duration is zero.
	ErrDurationZero = NewSlateError("Task duration of zero is not allowed.")

	// ErrExpirationInPast occurs when submitting a new schedule entry with an expiration in the past
	ErrExpirationInPast = NewSlateError("Schedule entry submitted with a Expires value in the past.")

	// ErrInvalidTiming occurs when submitting a new schedule entry with an expiration less than the begins time plus the every duration
	ErrInvalidTiming = NewSlateError("Schedule entry submitted cannot run due to Begins, Expires and Every values.")

	// ErrFunctionParamMismatch occurs when the fn params passed do not match the fn params required.
	ErrFunctionParamMismatch = NewSlateError("Mismatch in params passed to assigned Fn.")

	// ErrInvalidFunction occurs when the fn param bound to a schedule entry is not a valid func.
	ErrInvalidFunction = NewSlateError("Fn assigned not of type func.")

	// ErrMaximumRun occurs when a running count breaches the uint64 limit.
	ErrMaximumRun = NewSlateError("The runners Every ticker has reached the maximum uint64 number of ticks.")

	// ErrOrphanedEvent occurs when a running event cannot find the schedule that should contain it.
	ErrOrphanedEvent = NewSlateError("The executing event is no longer assigned to it's schedule. Orphaned.")

	// ErrScheduleEntryDuplicateID occurs when submitting a new schedule entry with a id that already exists on the schedule.
	ErrScheduleEntryDuplicateID = NewSlateError("Schedule entry submitted with a duplicate ID.")

	// ErrScheduleEntryNotFound occurs when unable to find a job runner by id.
	ErrScheduleEntryNotFound = NewSlateError("Scheduled entry not found.")

	// ErrScheduleNotFound occurs when unable to find a schedule by name.
	ErrScheduleNotFound = NewSlateError("Schedule not found.")
)

// Error() will return the error string value.
func (e slateError) Error() string {
	return e.s
}

// IsSlateError will return true on slateError instance.
func IsSlateError(err error) bool {
	_, ok := err.(*slateError)
	return ok
}

// NewSlateError will return a new SlateError instance.
func NewSlateError(errorMessage string) error {
	return &slateError{errorMessage}
}
