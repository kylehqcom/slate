# slate

slate _(verb)_ : `to plan that something will happen at a particular time in the future`

_slate_ is an in memory non persistent scheduler written in Go that simplifies handling repeat function calls.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kylehqcom/slate)](https://goreportcard.com/report/gitlab.com/kylehqcom/slate)

---

_slate_ allows you to

- run a scheduled event **n** number of times
- guarantees a scheduled event will not run before a predefined start time
- or a predefined expiration
- at every **n** unit of time (nano, micro, milli seconds, minutes...)
- with automatic expirations

But _slate_ also keeps track of these scheduled events running asynchronously in go routines
so you can forcefully stop them at anytime.

You can skip straight to the code and see a working [example/main.go](https://gitlab.com/kylehqcom/slate/-/blob/master/_example/main.go) or continue to read to see how to run a simple function **Every** second.

If you like this package or are using for your own needs, then \*star\*\* and let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)

## Install

First add the package with `go get gitlab.com/kylehqcom/slate`

```go
import "gitlab.com/kylehqcom/slate"

func Simple() {
    // Do some repeated work.
}

e := slate.ScheduleEntry{
    Fn:    Simple,
    Every: time.Duration(time.Second),
}
s := slate.NewSchedule()

ID, err := s.Submit(e)
if err != nil {
     return err
}

// Too easy! Now your scheduled entry will invoke the function Simple() every second.

// We can easily remove an individual scheduled entry by calling remove
s.Drop(ID)

// Or remove all scheduled entries with a reset
s.Reset()
```

## Schedules

Schedules hold and track all the submitted `ScheduleEntry` entries. Create a new schedule with an optional `ScheduleConfig` configuration.

```go
// Schedule created with default config options
s := slate.NewSchedule()

// To add config options to your New Schedule, you have a couple of options.
// Either make new from the ScheduleConfig struct

c := slate.ScheduleConfig{
	DisallowDuplicateEntryID bool
	FlushEvery               time.Duration
	EntryRetention           time.Duration
}

// Or call New with ScheduleConfigOption's
c := NewScheduleConfig(opts ...ScheduleConfigOption)

s, err := slate.NewSchedule(c)
```

The optional `FlushEvery` and `EntryRetention` fields help define a periodic background process which takes care of
cleaning up completed/expired `ScheduleEntry`'s for you. You can also manually remove your `ScheduleEntry` with

```go
// Remove a single schedule entry by the ScheduleEntry.ID
s.Drop(ID EntryID)

// Or remove all schedule entries on a schedule instance
s.Reset()
```

Both `s.Drop(ID EntryID)` and `s.Reset()` will first mark each `ScheduleEntry` as stopped so that the background processing
will halt accordingly. The `ScheduleEntry` is then removed from the schedule itself.

## Manager

If you have multiple schedules to keep track of, you can utilise the convenience `Manager` struct and funcs. Of particular importance is the `m.Reset()` func which will iterate over all `Schedules` bound to the manager and invoke `s.Reset()` on each.

```go
func NewManager() *Manager

// Add will add a schedule to this manager.
func (m *Manager) Add(name string, s *Schedule)

// All will return all Schedules on this manager.
func (m *Manager) All() Schedules

// Drop will stop and remove all related jobs on the schedule
// before removing the schedule from this manager.
func (m *Manager) Drop(name string)

// Get will return by name a schedule instance from this manager.
func (m *Manager) Get(name string) (*Schedule, error)

// Reset will stop and remove all schedules along with their associated entries.
func (m *Manager) Reset()
```

## ScheduleEntry

In _slate_, a `ScheduleEntry` defines all of the required values to schedule your function for execution.

```go
ScheduleEntry struct {
    // Ensure that this entry does not run before the begins time.
    Begins time.Time

    // Cap the number of times this entry will run.
    Cap uint64

    // Run this entry **every...
    Every time.Duration

    // Ensure that this entry does not run after the expires time.
    Expires time.Time

    // function to be called on **every. Wrap in closure for funcs with args.
    Fn func()

    // The generated or user assigned ID for this entry
    ID EntryID
}
```

Note that when assigning the `Begins` and/or `Expires` fields, the `time.Time` given **WILL NOT**
ensure that a job `Begins` or `Expires` exactly at the given `time.Time`. _slate_ **WILL**
guarantee however, that a `ScheduleEntry` will not run before or after the `time.Time` given. By default, the
Fn associated with the `ScheduleEntry` will be called immediately on submission, and on `Every` tick duration
thereafter. If you need to submit your schedule at a given time, then I suggest you check out the sister
package of [https://gitlab.com/kylehqcom/slater](https://gitlab.com/kylehqcom/slater) which leverages `slate`
and allows you to `SubmitAt` a future time.

As an example to better explain how the start time is determined:

If you assign an `Every` duration of an hour, a `Begins` of 12:00pm and `s.Submit(e ScheduleEntry)` the entry
at 10:50am, the first `Every` will occur immediately, skip, then again at roughly 11:50am and skip due to
the `Begins` of 12:00pm. The next `Every` will call the supplied `Fn` value at roughly 12:50pm. An `error`
will be returned on `ScheduleEntry` submission should the `Begins` be after the `Expires`, or the `Every`
duration added to the `Begins` is greater than `Expires`. By default both `Begins` and `Expires` are zero
valued and therefore ignored.

As _slate_ calls on every a Go closure/anon `func()`, you are required to wrap your own methods with receivers. Example

```go
// HasMany has x4 required params
func HasMany(x, y, z string, hit int64) {

// Wrap your funcs and bind to the ScheduleEntryand by calling
e := ScheduleEntry{
    Cap:   10,
    Fn:    func() {
               HasMany("www.kylehq.com", "slate", "is great", 24)
           },
    Every: time.Duration(time.Second),
}

ID, err := s.Submit(e)
```

#### EntryIDs

By default a unique `EntryID` is returned on `s.Submit(e ScheduleEntry)` enabling you to store and
track your `ScheduleEntry` progress. You can create directly by calling `slate.GenerateEntryID()`.
An `EntryID` is unique **ONLY** to each schedule. If you are creating numerous schedules (use
the provided manager), it may be beneficial to provide your own universal ID. Such as a `user_id`.

For example, imagine you have a broadcast `manager` with x3 schedules bound. One for push notifications,
one for sms notifications and another for emails. In such a scenario is would make sense to use a `user_id`
as your `EntryID` for each schedule. This gives you the flexibility to manipulate an entire schedule or
individual schedule entries pertaining to a specific user. Eg:

```go
// Add your `ScheduleEntry` instance with your Generated Entry ID
userEntryID := slate.GenerateEntryID("your user_id here")
e := ScheduleEntry {
    ID: userEntryID,
}
s := NewSchedule()
s.Submit(e)
m := NewManager()
m.Add("sms", s)

// Remove all `ScheduleEntry` instances from  the "sms" schedule.
s, err := m.Get("sms")

// Call Reset() on the Schedule to drop all `ScheduleEntry` instances from the schedule
s.Reset()

// Call DropSchedule() on the Manager to Reset() AND drop the schedule instance from the manager
m.Drop("sms")


// To remove all ScheduleEntry instances that have your userEntryID
for _, s := range m.All() {
    s.Drop(userEntryID)
}
```

## Common Questions

**Will a runner run forever?**

**No!** Internally _slate_ has a safety check based on a `uint64` value range. If the `ScheduleEntry` iterates to the `uint64`
limit, _slate_ will break from the running process. If you want/need to keep processing over the `uint64` limit, then simply
check the `EntryStats()` of the `ScheduleEntry` confirming the `State` and the `Err` of `ErrMaximumRun`. You can then re-add the
`ScheduleEntry` as required. But do you really need to execute you func _18446744073709551615_ times?

**TODO's**

- Profiling
