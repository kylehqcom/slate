package slate

import (
	"math"
	"sync"
	"time"
)

const (
	// stateCapBreached is a simple string representation of a cap breached state
	stateCapBreached runnerstate = "cap breached"

	// stateError is a simple string representation of an error state
	stateError runnerstate = "error"

	// stateExpired is a simple string representation of an expired state
	stateExpired runnerstate = "expired"

	// statePending is a simple string representation of a pending state
	statePending runnerstate = "pending"

	// stateRunning is a simple string representation of a running state
	stateRunning runnerstate = "running"

	// stateStopped is a simple string representation of a stopped state
	stateStopped runnerstate = "stopped"
)

type (
	// runner struct to contain meta data and the entry fn details.
	runner struct {
		completed    time.Time
		err          error
		entry        *ScheduleEntry
		fn           func()
		runningCount uint64
		started      time.Time
		stopper      chan struct{}
		stopped      time.Time
		state        runnerstate
		sync.Mutex
	}
	// runners are a map of all entry runners key'd on EntryID.
	runners map[EntryID]*runner

	runnerstate string
)

// Given a ScheduleEntry, create a runner instance with a schedule entries
// function and function params validated and bound
func createRunner(e *ScheduleEntry) (*runner, error) {
	fn := e.Fn
	if fn == nil {
		return nil, ErrInvalidFunction
	}

	return &runner{
		entry: e,
		fn:    fn,
		state: statePending,
	}, nil
}

// canBegin will return true if a begin time is set and is older than the current time.
func (r *runner) canBegin() bool {
	return r.entry.Begins.IsZero() || r.entry.Begins.Before(time.Now())
}

// capBreached will return true if this Job has a Cap and it is over or equal to the running count.
func (r *runner) capBreached() bool {
	r.Lock()
	defer r.Unlock()
	return (r.entry.Cap != 0 && r.runningCount >= r.entry.Cap)
}

// isCompleted will return true if a completed time is not zero/marked complete.
func (r *runner) isCompleted() bool {
	r.Lock()
	defer r.Unlock()
	return !r.completed.IsZero()
}

// isExpired will return true if an expiration time is set and is before now.
func (r *runner) isExpired() bool {
	return (!r.entry.Expires.IsZero() && r.entry.Expires.Before(time.Now()))
}

// isStopped returns this runners current stop flag value.
func (r *runner) isStopped() bool {
	r.Lock()
	defer r.Unlock()
	return !r.stopped.IsZero()
}

// Run invokes do() on this job runner instance via a go routine.
func (r *runner) run() {
	go do(r)
}

// Stop will stop execution of the ticker and assign the correct stopped state.
func (r *runner) stop() {
	r.Lock()
	defer r.Unlock()
	r.stopped = time.Now()
	r.state = stateStopped
	if r.stopper != nil {
		close(r.stopper)
	}
}

// do is called via run in a non blocking go routine and is responsible for processing the
// runner instance
func do(r *runner) {
	// Create the ticker based on the entry every value.
	t := time.NewTicker(r.entry.Every)
	defer func() {
		r.completed = time.Now().UTC()
		t.Stop()
	}()

	// Create the stopper channel that can be called from outside this go routine.
	r.Lock()
	r.stopper = make(chan struct{})
	r.Unlock()

	// Init the safety var used to ensure we do not tick more than the maximum uint value.
	safety := uint64(0)
	tick := func() bool {
		// Only continue processing if this job is not stopped.
		if r.isStopped() {
			return false
		}

		// If we have a start time, skip this tick if time not passed.
		if !r.canBegin() {
			return true
		}

		// Also check for an expiration time.
		if r.isExpired() {
			r.state = stateExpired
			return false
		}

		// Check for a Cap and whether it will breach.
		if r.capBreached() {
			r.state = stateCapBreached
			return false
		}

		// Set the started time if zero and running state
		if r.started.IsZero() {
			r.started = time.Now().UTC()
		}

		r.Lock()
		r.state = stateRunning
		r.Unlock()

		// Make the fn call in a non blocking go routine
		go r.fn()

		// Increment the safety and running counts.
		safety++
		r.runningCount++
		if safety == math.MaxUint64 {
			r.state = stateError
			r.err = ErrMaximumRun
			return false
		}

		// Additional safety checking if this entry is orphaned. No need
		// to process if no longer apart of the schedule
		_, err := r.entry.schedule.findRunner(r.entry.ID)
		if err != nil {
			r.state = stateError
			r.err = ErrOrphanedEvent
			return false
		}

		return true
	}

	for {
		// If not yet submitted, run now immediately and then on Every tick
		if !r.entry.submitted {
			r.entry.submitted = true
			if !tick() {
				return
			}
		}

		select {
		case <-t.C:
			if !tick() {
				return
			}
		case <-r.stopper:
			return
		}
	}
}
