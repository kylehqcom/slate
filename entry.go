package slate

import (
	"crypto/rand"
	"fmt"
	"time"
)

type (
	// EntryID is the identifier to reference a Schedule Entry
	EntryID string

	// ScheduleEntry is the default struct to assign running parameters.
	ScheduleEntry struct {
		// Ensure that this entry does not run before the begins time.
		Begins time.Time

		// Cap the number of times this entry will run.
		Cap uint64

		// Run this entry **every...
		Every time.Duration

		// Ensure that this entry does not run after the expires time.
		Expires time.Time

		// function to be called on **every. Wrap in closure for funcs with args.
		Fn func()

		// The generated or user assigned ID for this entry
		ID EntryID

		// An internal flag as to whether this scheduled entry has been submitted
		submitted bool

		// A reference back to the schedule this scheduleEntry is assigned
		schedule *Schedule
	}
)

// EntryID implements the Stringer interface
func (eid EntryID) String() string {
	return string(eid)
}

// GenerateEntryID will return a unique entry ID. You can always assign
// your own string ID to schedules. This is especially useful when using
// multiple schedules that pertain to a universal ID, such as a user_id.
func GenerateEntryID(ID ...string) EntryID {
	if len(ID) > 0 {
		return EntryID(ID[0])
	}

	unix32bits := uint32(time.Now().UTC().Unix())
	b := make([]byte, 12)
	numRead, err := rand.Read(b)
	if numRead != len(b) || err != nil {
		// On error return an empty string ID
		return EntryID("")
	}

	return EntryID(fmt.Sprintf("%x%x", unix32bits, b))
}
